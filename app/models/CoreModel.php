<?php
class CoreModel{
	private $db;
    public function __construct() {
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=dbname;charset=utf8','root','');

        }
        catch (PDOException $e){
            die($e);
        }
    }
    public function query($queryStr,$fetch, $single,$params = null){
        $query = $this->db->prepare($queryStr);
        try{
            $query->execute($params);
            if($fetch){
                $data = $query->fetchAll(PDO::FETCH_ASSOC);
                if(count($data) == 1 && $single) return $data[0]; 
                return $data;
            } 
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }        
    }
    public function insertArray($table, $data){ 
        $values = array_values($data);
        $keys = array_keys($data);        
        $pocetHodnot = count($values);
        $placeHolders = implode(',', array_fill(0, $pocetHodnot, '?'));
        $queryStr = 'INSERT INTO '.$table.' (' . implode(', ', $keys) . ')' . ' VALUES ('.$placeHolders.')';
        $insert = $this->db->prepare($queryStr);
        try{
            $insert->execute($values);
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }
    }
    public function updateArray($table, $data, $condition, $conditionValues){ 
        $values = array_merge(array_values($data), $conditionValues);
        $keys = array_keys($data);        
        foreach ($keys as &$key) {
            $key=$key.' = ?';
        }        
        $queryStr = 'UPDATE '.$table.' SET '.implode(', ', $keys).' '.$condition;
        $update = $this->db->prepare($queryStr);
        try{
            $update->execute($values);
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }
    }
}