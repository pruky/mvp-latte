<?php
class CorePresenter{
	protected $view;
	protected $model;
	protected $values = array();
	protected $tplFile;
	protected $user;
	public function __construct($pageName, $data = null){
		var_dump($data);
		$this->user = new User();
		$model = $pageName.'Model';
		$this->model = new $model();
		if(!isset($data['action'])){
			$this->tplFile = '../app/views/'.$pageName.'.latte';
			$this->wannaRender();
			$this->render($data);
			return;
		}
		$action = $data['action'].'Action';
		if(method_exists($this,$action)){
			unset($data['action']); 
			$this->$action($data);
		}
	}	
	public function wannaRender(){
		require_once('../vendor/latte.php');
		$this->view = new Latte\Engine;
		$this->view->setTempDirectory('../temp/');
	}
	public function redirectHome(){
		header('Location: ?page=homepage');
		die();
	}
	public function setMessage($type, $content){
		if(!isset($_COOKIE['messages'])){
			setcookie('messages', serialize(array($type => $content)),time()+3);
			return;
		}
		$data = unserialize($_COOKIE['messages']);
		$data[$type] = $content;
		setcookie('messages', serialize($data),time()+3);
	}
}