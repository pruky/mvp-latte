<?php
require '../app/User.php';
session_start();
function autoload($className){
	if(strpos($className,'Presenter') != false){
		require_once('../app/presenters/'.$className.'.php');
	}
	elseif(strpos($className,'Model') != false) {
		require_once('../app/models/'.$className.'.php');
	}
}
spl_autoload_register('autoload');
function createPage($data){
	$pageName = ucfirst($data['page']);
	unset($data['page']);
	$presenterName = $pageName.'Presenter';
	$presenter = new $presenterName($pageName,$data);
}
$array = array();
if(!isset($_GET['page'])){
	createPage(array('page'=>'homepage'));
	exit;	
}
$array = array_merge($_POST,$_GET);
createPage($array);


